import matplotlib.pyplot as plt
from numpy import linspace, pi, array, sin, sign, arange, cos


amplitude = 1
frequency = 1
signal_period = 1 / frequency
signal_resolution = 1000
number_of_signal_periods = 4
signal_duration = int(number_of_signal_periods*signal_period)
samples_per_signal_period = signal_resolution * number_of_signal_periods
period_sampling = signal_period / samples_per_signal_period
number_of_samples = int(1/period_sampling)+1
time = linspace(-2, 2, number_of_samples)
order_of_harmonics = int(
    input('Enter the order of harmonics to calculate:\t'))


colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
          '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
          '#bcbd22', '#17becf']


square_wave = lambda time:  (abs((time % 1) - 0.25) < 0.25).astype(float) - (abs((time % 1) - 0.75) < 0.25).astype(float)

plt.figure(1)
plt.plot(time, square_wave(time), colors[0])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Square signal')
plt.grid()
plt.savefig('Figure_1.png')


harmonic_values, harmonic_indexes = [], []
harmonic_sum, harmonic_order = 0, 1
harmonic_orders = order_of_harmonics + 1
while harmonic_order < harmonic_orders:
    if harmonic_order % 2 != 0:
        harmonic_value = 4 / (harmonic_order*pi) * \
            sin(2 * pi * harmonic_order * frequency * time)
        harmonic_values.append(harmonic_value)
        harmonic_indexes.append(harmonic_order)
        harmonic_sum += harmonic_value
    harmonic_order += 1
plt.figure(2)
plt.plot(time, square_wave(time), colors[0], label='Square signal')
plt.plot(time, harmonic_sum, colors[1],
         label=f'Harmonic orders: {order_of_harmonics}')
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Square signal approximation')
plt.legend()
plt.grid()
plt.savefig('Figure_2.png')


harmonic_order = 0
color_index = 0
harmonic_length = len(harmonic_indexes)
while harmonic_order < harmonic_length:
    plt.figure(3)
    plt.plot(time, harmonic_values[harmonic_order],
             colors[color_index], label=f'Harmonic order: {harmonic_indexes[harmonic_order]}')
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    plt.title('Square signal harmonic orders')
    plt.legend()
    plt.grid()
    color_index += 1
    harmonic_order += 1
    if color_index >= len(colors):
        color_index = 0
plt.savefig('Figure_3.png')


nth_harmonic_order_amplitude = lambda order:  2 / (pi * order)

harmonic_order_plane = array(harmonic_indexes)
plt.figure(4)
plt.stem(harmonic_order_plane, nth_harmonic_order_amplitude(
    harmonic_order_plane), label=f'Amplitude of {order_of_harmonics} harmonic orders')
plt.xlabel('Harmonic orders')
plt.ylabel('Amplitude')
plt.title(f'Amplitude of {order_of_harmonics} harmonic orders')
plt.legend()
plt.grid()
plt.savefig('Figure_4.png')

